/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.testcasew3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author user
 */
public class TestXo {

    public TestXo() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testCheckwin_No_Player_byX_output_false() {
        String[][] board = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};
        int row = 1;
        int col = 1;
        String player = "X";
        assertEquals(true, OXprogram.checkWin(board, player, row, col));
    }

    @Test
    public void testCheckwin_Row1_byX_output_true() {
        String[][] board = {{"X", "X", "X"}, {"_", "_", "_"}, {"_", "_", "_"}};
        String player = "X";
        int row = 1;
        int col = 1;
        assertEquals(true, OXprogram.checkWin(board, player, row, col));
    }

    @Test
    public void testCheckwin_Row2_byX_output_true() {
        String[][] board = {{"_", "_", "_"}, {"X", "X", "X"}, {"_", "_", "_"}};
        String player = "X";
        int row = 1;
        int col = 1;
        assertEquals(true, OXprogram.checkWin(board, player, row, col));
    }

    @Test
    public void testCheckwin_Row2_byO_output_true() {
        String[][] board = {{"_", "_", "_"}, {"O", "O", "O"}, {"X", "X", "_"}};
        String player = "O";
        int row = 1;
        int col = 1;
        assertEquals(true, OXprogram.checkWin(board, player, row, col));
    }

    @Test
    public void testCheckwin_Col1_byO_output_true() {
        String[][] board = {{"O", "_", "_"}, {"O", "_", "X"}, {"O", "X", "_"}};
        String player = "O";
        int row = 1;
        int col = 1;
        assertEquals(true, OXprogram.checkWin(board, player, col, row));
    }

    @Test
    public void testCheckwin_Col2_byX_output_true() {
        String[][] board = {{"O", "X", "_"}, {"O", "X", "O "}, {"O", "X", "_"}};
        String player = "O";
        int row = 1;
        int col = 1;
        assertEquals(true, OXprogram.checkWin(board, player, col, row));
    }

    @Test
    public void testCheckwin_Dia1_byX_output_true() {
        String[][] board = {{"X", "O", "_"}, {"O", "X", "_"}, {"O", "_", "X"}};
        String player = "O";
        int row = 1;
        int col = 1;
        assertEquals(true, OXprogram.checkWin(board, player, col, row));
    }

    @Test
    public void testCheckwin_Dia2_byO_output_true() {
        String[][] board = {{"_", "X", "O"}, {"O", "X", "_"}, {"X", "_", "O"}};
        String player = "O";
        int row = 1;
        int col = 1;
        assertEquals(true, OXprogram.checkWin(board, player, col, row));
    }
    
    @Test
    public void testCheckdraw_output_true() {
        String[][] board = {{"X", "X", "O"}, {"O", "X", "X"}, {"X", "O", "O"}};
        String player = "O";
        int row = 1;
        int col = 1;
        assertEquals(true, OXprogram.playerDraw(board, player, col, row));
    }

}
