/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testcasew3;

/**
 *
 * @author user
 */
class OXprogram {

    static boolean checkWin(String[][] board, String player, int row, int col) {
        if(checkRows(board, player, row, col)||checkCols(board, player, col, row) || checkDiagonals(board, player, col, row)){
            return true;
        }

        return false;
    }

    private static boolean checkRows(String[][] board, String player, int row, int col) {
         for (int j = 0; j < board[row - 1].length; j++) {
            if (!board[row - 1][j].toLowerCase().equals(player)) {
                return true;
            }
        }
        return false;
 
    }
    
      private static boolean checkCols(String[][] board, String player, int col, int row) {
         for (int j = 0; j < board[0].length; j++) {
            if (board[0][j].toLowerCase().equals(player) && board[1][j].toLowerCase().equals(player) && board[2][j].toLowerCase().equals(player)) {
                return true;
            }
        }
        return false;
    }
      public static boolean checkDiagonals(String[][] board, String player, int col, int row) {
        if (board[0][0].toLowerCase().equals(player) && board[1][1].toLowerCase().equals(player) && board[2][2].toLowerCase().equals(player)) {
            return true;
        }

        if (board[0][2].toLowerCase().equals(player) && board[1][1].toLowerCase().equals(player) && board[2][0].toLowerCase().equals(player)) {
            return true;
        }

        return false;
    }
      public static boolean playerDraw(String[][] board, String player, int col, int row){
        for(int i =0; i < board.length; i++){
            for(int j = 0; j< board.length;j++){
                if(board[i][j].contains("_")){
                    return false;
                }
            }
        }
        return true;
    }
    
}
